import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class SoftwareFrame extends JFrame {
	ManageTheString managestring = new ManageTheString();
	SoftwareTesting swt = new SoftwareTesting();
	public SoftwareFrame() {
		createFrame();
		//swt.setTestCase();
	}

	public void createFrame() {

		JFrame jf = new JFrame("S I R I M A N G K H A L A   C I N E M A");
		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		jf.setSize(700, 350);
		jf.setLayout(null);

		// MANAGE WINDOW
		JPanel panel2 = new JPanel();
		panel2.setBackground(Color.WHITE);
		panel2.setBorder(BorderFactory.createLineBorder(Color.black));
		panel2.setBounds(20, 20, 650, 280);
		panel2.setLayout(null);
		JPanel panelrd = new JPanel();
		panelrd.setBackground(Color.WHITE);
		panelrd.setBorder(BorderFactory.createLineBorder(Color.black));
		panelrd.setBounds(20, 20, 100, 100);
		panelrd.setLayout(null);

		// Create Textfield
		JTextArea jtq = new JTextArea("input word");
		jtq.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		jtq.setBounds(10, 10, 300, 80);

		JTextArea jtnum = new JTextArea("input number");
		jtnum.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		jtnum.setBounds(10, 100, 300, 25);

		JTextArea jtans = new JTextArea();
		jtans.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		jtans.setBounds(340, 10, 300, 100);
		jtans.setLineWrap(true);

		// Create text label

		JLabel p = new JLabel("PLEASE SELECT");
		p.setBounds(50, 130, 300, 25);

		// Create Button
		JButton sb = new JButton("S U B M I T");
		sb.setBounds(250, 210, 150, 30);

		// Create rdo

		JComboBox box = new JComboBox();
		box.setBounds(50, 160, 200, 30);
		box.addItem("Split");
		box.addItem("N-Gram");
		box.addItem("Both Split&N-Gram");

		// addActionListener
		sb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String input = jtq.getText();
				String inputnum = jtnum.getText();
				String select = (String) box.getSelectedItem();
				System.out.println(input);
				if (select == "Split") {
					jtans.setText("Number of word is "+ managestring.split(input));
				} else if (select == "N-Gram") {
					int num = Integer.parseInt(inputnum);
					jtans.setText("The answer is "+ managestring.ngram(input, num));
				} else if (select == "Both Split&N-Gram") {
					int num = Integer.parseInt(inputnum);
					jtans.setText("Number of word is "+ managestring.split(input) + " // The answer is "+ managestring.ngram(input, num));
				}
			}
		});

		// add
		jf.add(panel2);

		panel2.add(box);
		panel2.add(sb);
		panel2.add(p);
		panel2.add(jtq);
		panel2.add(jtans);
		panel2.add(jtnum);

		jf.setVisible(true);
		jf.setResizable(false);

	}

}
