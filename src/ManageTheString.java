import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;

public class ManageTheString {
	public int split(String word) {
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer tokens = new StringTokenizer(word);
		String str = new String();
		while (tokens.hasMoreElements()) {
			String mystring = (String) tokens.nextElement();
			list.add(mystring);
			str += mystring;
		}
		int countWord = list.size();
		System.out.println(list);
		return countWord;
	}

	public String ngram(String word, int n) {
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer tokens = new StringTokenizer(word);
		String str = new String();
		String str2 = new String();
		// cut space out and attach the word
		while (tokens.hasMoreElements()) {
			String mystring = (String) tokens.nextElement();
			list.add(mystring);
			str += mystring;
		}

		ArrayList<String> listngram = new ArrayList<String>();

		while (str.length() > n - 1) {
			if (listngram.contains(str.substring(0, n)))
				str = str.substring(1, str.length());
			else
				listngram.add(str.substring(0, n));
		}
		for (int i = 0; i < listngram.size(); i++) {
			if (i == listngram.size() - 1) {
				str2 += listngram.get(i);
			} else
				str2 += listngram.get(i) + ",";
		}
		return str2;
	}

}
